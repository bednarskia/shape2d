
extern crate core;
extern crate Vec2d;
extern crate serialize;
use core::ops::Add;
use std::vec::Vec as Vec;
use core::ops::Sub;
use Vec2d::vec2d::V2d as V2d;
use std::num::Float;
use core::f64::consts::PI as PI;
use std::f64::EPSILON as EPSILON;
#[derive(Encodable)]
#[derive(Decodable)]
pub struct Shape
{
	pub lines: Vec<LineSegment>,
	pub circles: Vec<CircleSegment>,
}

impl Shape
{
	/**Intersection,TanPlane**/
	pub fn	check_collision(&mut self,start: V2d, end : V2d)-> Option<(V2d,V2d)>
	{
	
		let pos = new_line_segment(start,end);
		for ls in self.lines.iter_mut()
		{
			//make sure to avoid sticking....
			if ls.time_out
			{
				ls.time_out = false;
				continue;
			}
			let o = ls.check_collision(&pos);
			match o 
			{
				Some(_)=>
				{ 	//can't collide with the same segment on two consecutive timesteps
					ls.time_out=true;
					return o;
				}
				None => {continue;}
			}	
		}
		for cs in self.circles.iter_mut()
		{

			if cs.time_out
			{//no colliding with the same circle in two consecutive timesteps, technically could lead to error
				cs.time_out = false;
				continue;
			}
			let o = cs.check_collision(&pos);
			match o 
			{
				Some(_)=> 
				{
					cs.time_out=true;
					return o;
				}
				None => {continue;}
			}	
		}
		return None;
	}
	
}

#[derive(Encodable)]
#[derive(Decodable)]
pub struct LineSegment
{
	start :V2d,
	end :V2d,
	m :Option<f64>,
	time_out:bool,
}

fn slope(start :&V2d, end :&V2d) ->Option<f64>
{
	let run = end.x - start.x;
	match run
	{
		0.0 => {return None;}
		_ => {return Some((end.y-start.y)/run);}
	}
	//hurrah for exhaustive matching? Did you say why not use an if? sorry I can't hear you....
}

pub fn new_line_segment(start :V2d, end :V2d) -> LineSegment
{
	let ret = LineSegment
	{
		start:start,
		end:end, 
		m: slope(&start,&end),
		time_out:false,
	};
	return ret;
}
impl LineSegment
{
	/** takes in two line segments, if they intersect return intersection point and tangent vector
		NOTe: if the start point of the passed segment is on the self segment that is not considered a collision
		because I assume the checker got it last time and changed velocities.
	**/
	fn check_collision(&self, other :&LineSegment)-> Option<(V2d,V2d)>
	{	
		
	/*	if self.on_line(&other.start)
		{
			println!("Starting on line, bailing early");
			return None;
		}*/
		
		//check the interception
		match self.intersection(other)
		{
			None => {return None;}
			//check if the point lays on the line, but first make sure it isn't first point
			Some(v) => 
			{

				if  !self.on_line(&v) ||  !other.on_line(&v)
				{
						return None;
				}
				//println!("&&&&&&&&&&&&&&&&&&&&&&&&On self and other&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
				return Some((v,self.start.sub(&self.end)));//if it does return point, tangent vec....
			}


		}
	}

	/**	returns point of intersection between  two lines defined by segments
		returned point is not neccessarilly on either line.......
	**/
	fn intersection(&self, other :&LineSegment) -> Option<V2d>
	{
		//kill the paralell case
		if (self.m == other.m) || (self.m.is_none()&&other.m.is_none())
		{
			return None;
		}else if self.m.is_none()
		{//self is vert, other isn't
			let x = self.start.x;//same as self.end.x
			let y = other.m.unwrap()*(x - other.start.x) + other.start.y;
			return Some(V2d::new(x,y));
		}else if other.m.is_none()
		{//other is vert this isn't
			let x = other.start.x;//same as other.end.x
			let y = self.m.unwrap()*(x - self.start.x) + self.start.y;
			return Some(V2d::new(x,y));
			
		}

		//main case using formula m(x-x0) = y-y0, but for two different lines, and solved
		let m0 = self.m.unwrap();
		let m1 = other.m.unwrap();
		let x = (m0*self.start.x - m1*other.start.x + other.start.y - self.start.y)/(m0 - m1);
		let y = self.m.unwrap()*(x-self.start.x) + self.start.y;
		let y1 = other.m.unwrap()*(x-other.start.x) + other.start.y;
		let ret = V2d::new(x,y);
		return Some(ret);
	}
	/**
		returns true if the passed points end points is between line endpoints, it is assumed slope already matches
	**/
	fn on_line(&self, pt :&V2d) -> bool
	{
		match self.m
		{
			Some(slope) =>
			{
				return between(self.start.x,self.end.x,pt.x) && between(self.start.y,self.end.y,pt.y);
			}
			None => 
			{
				return (self.start.x - pt.x).abs() <= EPSILON && between(self.start.y, self.end.y, pt.y);
			}
		} 
	}
	//moves this segment's origin	
	fn translate(&self, pt :&V2d)->LineSegment
	{
		LineSegment
		{
			start: self.start.sub(pt),
			end: self.end.sub(pt),
			m:self.m,//translating does not change slope
			time_out:false,
		}
	}
}	

/**Checks whether b is betwixt s and e**/
fn between(s :f64, e :f64, b :f64) -> bool
{
	return (s <= b && b <= e) || (e <= b && b <= s);
}
//phi = 0..2*pi
#[derive(Encodable)]
#[derive(Decodable)]
pub struct CircleSegment
{
	center :V2d,
	radius :f64,
	phi0 :f64,
	phi1 :f64,
	time_out:bool,

}
pub fn new_circle_segment(center :V2d, radius :f64, phi0 :f64, phi1 :f64)->CircleSegment
{
	CircleSegment
	{
		center:center,
		radius:radius,
		phi0:phi0,
		phi1:phi1,
		time_out:false,
	}
}
impl CircleSegment
{
	pub fn	check_collision(&self,line :&LineSegment)-> Option<(V2d,V2d)>
	{
		let l = line.translate(&self.center);
		//check if start point is exactly on radius
		let rsq = self.radius*self.radius;
		/*let sxsq = l.start.x*l.start.x;
		let sysq = l.start.y*l.start.y;
		let exsq = l.end.x*l.end.x;
		let eysq = l.end.y*l.end.y;*/
		/*if (rsq - sysq - sxsq).abs() < EPSILON//trying to kill float stuff
		{
			println!("bailing on da circle");
			return None;
		}*/

		let smagsq = l.start.magsq();
		let emagsq = l.end.magsq();
		if ( smagsq < rsq && emagsq  < rsq) || (smagsq > rsq && emagsq > rsq) 
		{//both  points on same side of border
			return None;
		}
		//collision definetly happened now
		//println!("def been a circle collision if the phi is right");
		match l.m
		{
			Some(m) =>
			{
				let B = l.start.y - l.start.x*m;

				let a = m*m + 1.0;
				let b = 2.0*m*B;
				let c = B*B - rsq;
				match quad_solve(a,b,c)
				{
					(None,None) =>
					{
						println!("Huh, no intersect after all, something is screwy");
						return None;
					}
					(Some(x),None) =>
					{
						if !self.check_x(x,&l)
						{
							return None;
						}
						let pt = V2d::new(x,m*x+B);
						return Some((pt.add(&self.center),self.tan_vec(&pt)));

					}
					(None,Some(x)) =>
					{
						if !self.check_x(x,&l)
						{
							return None;
						}
						let pt = V2d::new(x,m*x+B);
						return Some((pt.add(&self.center),self.tan_vec(&pt)));
					}
					(Some(x0),Some(x1)) =>
					{
						let isx0 = self.check_x(x0,&l);
						let isx1 = self.check_x(x1,&l);
						if (!isx0) && (!isx1)
						{
							return None;
						}
						let x =
						if !isx0{x1}
						else if !isx1{x0}
						else if (l.start.x - x0).abs() > (l.start.x-x1).abs() {x1}
						else {x0};	
						let pt = V2d::new(x,m*x+B);
						return Some((pt.add(&self.center),self.tan_vec(&pt)));
					}
				}
			}
			None =>
			{
				let x = l.start.x;
				let y0 = self.radius*self.radius - x*x;
				let y1 = -y0;
				let b0 = self.check_y(&l,x,y0);
				let b1 = self.check_y(&l,x,y0);

				if !b0 && !b1
				{
					return None;
				}
				let y = if !b0
				{
					y1
				} else if !b1{
					y0
				} else if (y0-l.start.y).abs()>(y1-l.start.y).abs(){
					y1
				}else{y0};
				let pt = V2d::new(x,y);
				return Some((pt.add(&self.center),self.tan_vec(&pt)));
			}
		}
		return None;
	}
	fn check_phi(&self,x:f64,y:f64)->bool
	{
		let mut phi = y.atan2(x);
		if phi < 0.0
		{
			phi+=2.0*PI;
		}
		if self.phi0 < self.phi1//phi0 is smallest
		{
			return (self.phi0 <= phi)&&(phi <= self.phi1);
	
		}
		//circle has wrapped the origin......
		//so we must split the checking into two intervals Phi0..2*Pi, 0..Phi1
		return ((self.phi0 <= phi) && (phi <= 2.0*PI))||((0.0 <=phi) &&(phi<=self.phi1));
		
	}
	fn check_y(&self,l:&LineSegment,x:f64,y:f64) ->bool
	{
		if ! between(l.start.y,l.end.y,y)
		{
			return false;
		}
		return self.check_phi(x,y);
	}
	/**takes in a translated segment and checks whether the x spat out is a valid answer**/
	fn check_x(&self, x:f64, l:&LineSegment) ->bool
	{
		//we have an x now for the y
		let y = l.m.unwrap()*(x-l.start.x) + l.start.y;
		if !between(l.start.x, l.end.x,x)//as it is a line, I only need to check x as y is already on the line...
		{
			return false;
		}
		return self.check_phi(x,y);
		
	}
	
	/**Takes a translated point, and returns an untranslated tangent vector.
	**/
	fn tan_vec(&self, p :&V2d) -> V2d
	{
				if p.x == self.radius//vert line
                                {
                                        V2d::new(0.0,1.0)
                                }else if p.x== -self.radius {
                                        V2d::new(0.0,-1.0)
                                }else if  p.y < 0.0{
                                        let m =  -p.x/((self.radius*self.radius-p.x*p.x).sqrt());
                                        V2d::new(-1.0,m)
                                }else{
                                        let m =  -p.x/((self.radius*self.radius-p.x*p.x).sqrt());
                                        V2d::new(1.0,m)
                                }

	}
}
//the two "real" results
fn quad_solve(a :f64, b:f64, c:f64) ->(Option<f64>,Option<f64>)
{
	let sq = (b*b-4.0*a*c).sqrt();
        let s1 = (-b + sq)/(2.0*a);
        let s2 = (-b-sq)/(2.0*a);
        let r1 =

	if !s1.is_infinite() && !s1.is_nan()
	{
		Some(s1)
	} else{
		None
	};
	let r2 = 
	if !s2.is_infinite() && !s2.is_nan()
	{
		Some(s2)
	} else{
		None
	};
	return (r1,r2);
}
